﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyData : MonoBehaviour 
{
  public Image enemyImage;
  public Text enemyName;
  public Text droppedItem;
  public Transform weakness;
  public Text element;
}
