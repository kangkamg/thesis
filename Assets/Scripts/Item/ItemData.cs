﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemData : MonoBehaviour 
{
  public Item items;
  public int ID;
  public int amount;
}
