﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Const 
{
  public static string PreviousScene = "PREVIOUSSCENE";
  public static string MapNo = "MAPNO";
  public static string InTownScene = "INTOWNSCENE";
  public static string TownSceneNo = "TOWNSCENENO";
  public static string SaveAmount = "SAVEAMOUNT";
  public static string Language = "LANGUAGE";
  public static string IsTutorialDone = "ISTUTORIALDONE"; 
  
  public static string WhatOpenInMenuScene = "WHATOPENINMENUSCENE";
  public static string OpenSupMenuScene = "OPENSUPMENUSCENE";
  public static string StoryType = "STORYTYPE";

  public static string Version = "VERSION";
  public static string NewGame = "NewGame";
}
