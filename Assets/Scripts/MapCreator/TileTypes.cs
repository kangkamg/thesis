﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileTypes
{
  Grass,
  Water,
  WaterDeep,
  Ground,
  Rock,
  Normal
}
